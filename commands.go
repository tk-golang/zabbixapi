package main

import (
	"github.com/mitchellh/cli"
	//"github.com/tk/zabbixapi/command"
	"zabbixapi/command"
)

func Commands(meta *command.Meta) map[string]cli.CommandFactory {
	return map[string]cli.CommandFactory{
		"host get": func() (cli.Command, error) {
			return &command.HostGetCommand{
				Meta: *meta,
			}, nil
		},
		"host delete": func() (cli.Command, error) {
			return &command.HostDeleteCommand{
				Meta: *meta,
			}, nil
		},
		"host update group": func() (cli.Command, error) {
			return &command.HostUpdateGroupCommand{
				Meta: *meta,
			}, nil
		},
		"host update template": func() (cli.Command, error) {
			return &command.HostUpdateTemplateCommand{
				Meta: *meta,
			}, nil
		},
		"group get": func() (cli.Command, error) {
			return &command.GroupGetCommand{
				Meta: *meta,
			}, nil
		},
		"template get": func() (cli.Command, error) {
			return &command.TemplateGetCommand{
				Meta: *meta,
			}, nil
		},
		"graph get": func() (cli.Command, error) {
			return &command.GraphGetCommand{
				Meta: *meta,
			}, nil
		},
		"screen get": func() (cli.Command, error) {
			return &command.ScreenGetCommand{
				Meta: *meta,
			}, nil
		},
		"screen create": func() (cli.Command, error) {
			return &command.ScreenCreateCommand{
				Meta: *meta,
			}, nil
		},
		"screen delete": func() (cli.Command, error) {
			return &command.ScreenDeleteCommand{
				Meta: *meta,
			}, nil
		},

		"version": func() (cli.Command, error) {
			return &command.VersionCommand{
				Meta:     *meta,
				Version:  Version,
				Revision: GitCommit,
				Name:     Name,
			}, nil
		},
	}
}
