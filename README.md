# zabbixapi



## Description

## Usage

* create auth config file

  $ vi ~/.config/zabbixapi.yml 
  url: http://ZABBIX_HOST/zabbix/api_jsonrpc.php
  user: USER_NAME
  password: PASSWORD

host get
   全ホスト一覧取得

host get [HOST_NAME]
   ホスト情報取得

host delete [HOST_NAME]
   ホスト設定を削除

host update group  [HOST_NAME] [GROUP_NAME]
   ホストにグループ設定

host update template [HOST_NAME] [TEMPLATE_NAME]
   ホストにテンプレート設定

graph get [HOST_NAME]
   ホストで設定されているグラフID, グラフ名取得

group get
   グループ一覧

template get 
   テンプレート一覧

template get [HOST_NAME]
   ホストがリンクしているテンプレート一覧

## Install

To install, use `go get`:

```bash
$ go get -d github.com/tk/zabbixapi
```


## Author

[tk](https://gitlab.com/tk20160809)
