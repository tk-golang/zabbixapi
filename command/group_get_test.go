package command

import "fmt"

func ExampleGroupGetCommand_makeParams() {
	c := &GroupGetCommand{
		Meta: *meta,
	}

	// len(args) != 1
	data, _ := c.makeParams([]string{"www01"})
	fmt.Printf("%+v\n", data)

	// len(args) == 1
	data, _ = c.makeParams([]string{})
	fmt.Printf("%+v\n", data)

	// Output:
	// &{Auth: Jsonrpc:2.0 Method:hostgroup.get ID:1 Params:{Filter:{Name:[www01]}}}
	// &{Auth: Jsonrpc:2.0 Method:hostgroup.get ID:1 Params:{Filter:{Name:[]}}}

}

func ExampleGroupGetCommand_GetGroupID() {

	c := &GroupGetCommand{
		Meta: *meta,
	}
	data, _ := c.GetGroupID("www01")
	fmt.Printf("%+v\n", data)

	// Output:
	// 22

}

func ExampleGroupGetCommand_GroupGetAPI() {

	c := &GroupGetCommand{
		Meta: *meta,
	}
	data, _ := c.GroupGetAPI([]string{"www01"})
	fmt.Printf("%+v\n", data)

	// Output:
	// &{Result:[{Groupid:22 Name:Linux servers}]}

}
