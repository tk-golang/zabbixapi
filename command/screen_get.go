package command

import (
	"encoding/json"
	"fmt"
	"strings"
)

type ScreenGetCommand struct {
	Meta
}

type reqScreenGet struct {
	Auth    string `json:"auth"`
	Jsonrpc string `json:"jsonrpc"`
	Method  string `json:"method"`
	ID      int    `json:"id"`
	Params  struct {
		Output string `json:"output"`
		Filter struct {
			Screenid []string `json:"screenid"`
			Name     []string `json:"name"`
		} `json:"filter"`
	} `json:"params"`
}

type resScreenGet struct {
	Jsonrpc string `json:"jsonrpc"`
	ID      int    `json:"id"`
	Result  []struct {
		Screenid string `json:"screenid"`
		Name     string `json:"name"`
	} `json:"result"`
}

func (c *ScreenGetCommand) makeParams(args []string) (*reqScreenGet, int) {

	params := &reqScreenGet{
		Jsonrpc: JSONRPC,
		Method:  SCREEN_GET_METHOD,
		ID:      ID,
		Auth:    c.Meta.Key,
	}
	params.Params.Output = "extend"

	if len(args) == 1 {
		name := args[0]
		params.Params.Filter.Name = append(params.Params.Filter.Name, name)
	}

	return params, 0
}

func (c *ScreenGetCommand) ScreenCheck(name string) int {

	data, ret := c.ScreenGetAPI([]string{name})
	if ret != 0 {
		c.Ui.Error(fmt.Sprintf("ScreenGetAPI: %s", name))
		return 1
	}
	if len(data.Result) != 0 {
		c.Ui.Error(fmt.Sprintf("Screen exist: %s", name))
		return 1
	}
	return 0
}

func (c *ScreenGetCommand) ScreenGetID(name string) (string, int) {

	data, ret := c.ScreenGetAPI([]string{name})
	if ret != 0 {
		c.Ui.Error(fmt.Sprintf("ScreenGetAPI: %s", name))
		return "", 1
	}
	if len(data.Result) == 0 {
		c.Ui.Error(fmt.Sprintf("Screen does not exist: %s", name))
		return "", 1
	}
	return data.Result[0].Screenid, ret
}

func (c *ScreenGetCommand) ScreenGetAPI(args []string) (*resScreenGet, int) {

	params, ret := c.makeParams(args)
	if ret != 0 {
		return &resScreenGet{}, ret
	}

	var data resScreenGet
	res, err := zabbixRequest(c.Meta.URL, params)
	if err != nil {
		c.Ui.Error(fmt.Sprintf("zabbixRequest: %s", err))
		return &resScreenGet{}, 1
	}

	perr := json.Unmarshal([]byte(res), &data)
	if perr != nil {
		c.Ui.Error(fmt.Sprintf("json unmarshal: %s", err))
		return &resScreenGet{}, 1
	}

	return &data, 0

}

func (c *ScreenGetCommand) Run(args []string) int {

	data, ret := c.ScreenGetAPI(args)
	if ret != 0 {
		return ret
	}

	for _, v := range data.Result {
		fmt.Printf("%s %s\n", v.Screenid, v.Name)
	}

	return 0
}

func (c *ScreenGetCommand) Synopsis() string {
	return "get screen info"
}

func (c *ScreenGetCommand) Help() string {
	helpText := `
Usage:
   * all screen
     screen get 

   * single screen
     screen get [SCREEN_NAME]

`
	return strings.TrimSpace(helpText)
}
