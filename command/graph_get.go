package command

import (
	"encoding/json"
	"fmt"
	"strings"
)

type GraphGetCommand struct {
	Meta
}

type reqGraphGet struct {
	Auth    string `json:"auth"`
	Jsonrpc string `json:"jsonrpc"`
	Method  string `json:"method"`
	ID      int    `json:"id"`
	Params  struct {
		Hostids string `json:"hostids"`
	} `json:"params"`
}

type resGraphGet struct {
	Result []struct {
		Graphid string `json:"graphid"`
		Name    string `json:"name"`
	} `json:"result"`
}

// ホストのグラフ一覧を返す
func (c *GraphGetCommand) GraphGetids(hostid string) ([]string, int) {

	data, ret := c.GraphGetAPI([]string{hostid})
	if ret != 0 {
		return []string{}, 1
	}
	if len(data.Result) == 0 {
		c.Ui.Error(fmt.Sprintf("Graph does not exist: %s", hostid))
		return []string{}, 1
	}
	var graphids []string
	for _, v := range data.Result {
		graphids = append(graphids, v.Graphid)
	}
	return graphids, 0

}

func (c *GraphGetCommand) makeParams(args []string) (*reqGraphGet, int) {

	if len(args) != 1 {
		fmt.Println("graph get [HOST_NAME]")
		return &reqGraphGet{}, 1
	}
	name := args[0]
	h := &HostGetCommand{
		Meta: c.Meta,
	}
	hostid, ret := h.HostGetID(name)
	if ret != 0 {
		return &reqGraphGet{}, 1
	}

	params := &reqGraphGet{
		Jsonrpc: JSONRPC,
		Method:  GRAPH_GET_METHOD,
		ID:      ID,
		Auth:    c.Meta.Key,
	}
	params.Params.Hostids = hostid

	return params, 0

}

func (c *GraphGetCommand) GraphGetAPI(args []string) (*resGraphGet, int) {

	params, ret := c.makeParams(args)
	if ret != 0 {
		return &resGraphGet{}, ret
	}

	res, err := zabbixRequest(c.Meta.URL, params)
	if err != nil {
		c.Ui.Error(fmt.Sprintf("zabbixRequest: %s", err))
		return &resGraphGet{}, 1
	}

	var data *resGraphGet
	perr := json.Unmarshal([]byte(res), &data)
	if perr != nil {
		c.Ui.Error(fmt.Sprintf("Error json.Unmarshal: %s", err))
		return data, 1
	}

	return data, 0
}

func (c *GraphGetCommand) Run(args []string) int {

	data, ret := c.GraphGetAPI(args)
	if ret != 0 {
		return ret
	}

	for _, v := range data.Result {
		fmt.Printf("%v %v\n", v.Graphid, v.Name)
	}

	return 0
}

func (c *GraphGetCommand) Synopsis() string {
	return "Return only graphs that belong to the given host"
}

func (c *GraphGetCommand) Help() string {
	helpText := `
Usage:
   graph get [HOST_NAME]

`
	return strings.TrimSpace(helpText)
}
