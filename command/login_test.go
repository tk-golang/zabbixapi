package command

import (
	"fmt"
	//"os"
)

/*
 * TestableExamples
 *    Example をそのままテストコードとして記述
 *    関数は Example から始まる名前で定義し、
 *    出力を // Output: から始まるコメントを書くことで出力内容と比較してテストする
 *    // Output: はメソッドの最後に記述する
 *
 */

// JSON のレスポンスから authkey を取得するテスト
func ExampleLoginUnmarshal() {

	response := `{"jsonrpc": "2.0", "result": "0424bd59b807674191e7d77572075f33", "id": 1}`
	//response := LoginUnmarshalResponse()

	result, _ := loginUnmarshal(response)
	fmt.Println(result)
	// Output: 0424bd59b807674191e7d77572075f33

}

// ダミーコンフィグファイル test/.config/zabbixapi.ymlを読み込むテスト
func ExampleLoadConfig() {

	//os.Setenv("HOME", "./test")

	config, _ := LoadConfig()
	fmt.Println(config.URL)
	// Output: http://dummy
}
