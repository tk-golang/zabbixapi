package command

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"
)

type JsonRPCRequest struct {
	Jsonrpc string      `json:"jsonrpc"`
	Method  string      `json:"method"`
	Params  interface{} `json:"params"`

	// Zabbix 2.0:
	// The "user.login" method must be called without the "auth" parameter
	Auth string `json:"auth,omitempty"`
	Id   int    `json:"id"`
}

/*
 インターフェース
 構造体のメソッドとして渡すことにより、
 同一名の関数を別の名前空間にとして
 異なる構造体をZabbixRequest に渡せる
*/
type ParamsToJSON interface{}

func ToJSON(p ParamsToJSON) (string, error) {
	json, jerr := json.Marshal(p)
	if jerr != nil {
		return "", fmt.Errorf(`*** Failed to parse the JSON: %v`, p)
	}
	return string(json), nil
}

/*
 * ZabbixRequest https://www.zabbix.com/documentation/3.0/manual/api/reference/host/get

 * url zabbix api url
 * params
 *
   1. params 構造体を json に変換
   2. url へ POST リクエスト
   3. http status code 200 確認
*/

func zabbixRequest(url string, params ParamsToJSON) (string, error) {

	json, werr := ToJSON(params)
	if werr != nil {
		return "", werr
	}
	req, qerr := http.NewRequest("POST", url, bytes.NewBuffer([]byte(json)))
	if qerr != nil {
		return "", qerr
	}
	req.Header.Add("Content-type", "application/json-rpc")

	client := &http.Client{}
	res, serr := client.Do(req)
	if serr != nil {
		return "", serr
	}
	// 関数を抜ける際に必ずresponseをcloseするようにdeferでcloseを呼ぶ
	defer res.Body.Close()

	// func ReadAll(r io.Reader) ([]byte, error)
	body, rerr := ioutil.ReadAll(res.Body)
	if rerr != nil {
		return "", rerr
	}

	if res.Status != "200 OK" {
		return "", errors.New(res.Status)
	}

	return string(body), nil

}
