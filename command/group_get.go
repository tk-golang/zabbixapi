package command

import (
	"encoding/json"
	"fmt"
	"strings"
)

type GroupGetCommand struct {
	Meta
}

type reqGroupGet struct {
	Auth    string `json:"auth"`
	Jsonrpc string `json:"jsonrpc"`
	Method  string `json:"method"`
	ID      int    `json:"id"`
	Params  struct {
		Filter struct {
			Name []string `json:"name"`
		} `json:"filter"`
	} `json:"params"`
}

type resGroupGet struct {
	Result []struct {
		Groupid string `json:"groupid"`
		Name    string `json:"name"`
	} `json:"result"`
}

func (c *GroupGetCommand) makeParams(args []string) (*reqGroupGet, int) {

	params := &reqGroupGet{
		Jsonrpc: JSONRPC,
		Method:  GROUP_GET_METHOD,
		ID:      ID,
		Auth:    c.Meta.Key,
	}

	if len(args) == 1 {
		name := args[0]
		params.Params.Filter.Name = append(params.Params.Filter.Name, name)
	}

	return params, 0
}

// グループ名を受け取りグループIDを返す
func (c *GroupGetCommand) GetGroupID(name string) (string, int) {

	data, ret := c.GroupGetAPI([]string{name})
	if ret != 0 {
		return "", 1
	}
	if len(data.Result) == 0 {
		c.Ui.Error(fmt.Sprintf("Group does not exist: %s", name))
		return "", 1
	}
	return data.Result[0].Groupid, 0
}

func (c *GroupGetCommand) GroupGetAPI(args []string) (*resGroupGet, int) {

	params, ret := c.makeParams(args)
	if ret != 0 {
		return &resGroupGet{}, ret
	}

	res, err := zabbixRequest(c.Meta.URL, params)
	if err != nil {
		c.Ui.Error(fmt.Sprintf("zabbixRequest: %s", err))
		return &resGroupGet{}, 1
	}

	var data *resGroupGet
	err = json.Unmarshal([]byte(res), &data)
	if err != nil {
		c.Ui.Error(fmt.Sprintf("Error json.Unmarshal: %s", err))
		return data, 1
	}

	return data, 0
}

func (c *GroupGetCommand) Run(args []string) int {

	data, ret := c.GroupGetAPI(args)
	if ret != 0 {
		return ret
	}

	for _, v := range data.Result {
		fmt.Printf("%v %v\n", v.Groupid, v.Name)
	}

	return 0
}

func (c *GroupGetCommand) Synopsis() string {
	return "group list"
}

func (c *GroupGetCommand) Help() string {
	helpText := `
Usage:
   group get [HOST_NAME]

`
	return strings.TrimSpace(helpText)
}
