package command

import (
	"encoding/json"
	"fmt"
	"strings"
)

type HostUpdateTemplateCommand struct {
	Meta
}

type reqHostUpdateTemplate struct {
	Auth    string `json:"auth"`
	Jsonrpc string `json:"jsonrpc"`
	Method  string `json:"method"`
	ID      int    `json:"id"`
	Params  struct {
		Hostid    string       `json:"hostid"`
		Templates []Templateid `json:"templates"`
	} `json:"params"`
}

type Templateid struct {
	Templateid string `json:"templateid"`
}

type resHostUpdateTemplate struct {
	Result struct {
		Hostids []string `json:"hostids"`
	} `json:"result"`
}

func (c *HostUpdateTemplateCommand) makeParams(args []string) (*reqHostUpdateTemplate, int) {

	params := &reqHostUpdateTemplate{
		Jsonrpc: JSONRPC,
		Method:  HOST_UPDATE_METHOD,
		ID:      ID,
		Auth:    c.Meta.Key,
	}
	if len(args) != 2 {
		fmt.Println("host update template [HOST_NAME] [TEMPLATE_NAME]")
		return params, 1
	}

	// host check
	name := args[0]
	h := &HostGetCommand{
		Meta: c.Meta,
	}
	hostid, ret := h.HostGetID(name)
	if ret != 0 {
		return params, ret
	}
	template_name := args[1]
	g := &TemplateGetCommand{
		Meta: c.Meta,
	}
	templateid, ret := g.GetTemplateID(template_name)
	if ret != 0 {
		return params, ret
	}

	// 既存のテンプレート設定は保持
	templateids, ret := g.GetTemplateIDs(name)
	if ret != 0 {
		return params, ret
	}
	for _, v := range templateids {
		params.Params.Templates = append(params.Params.Templates, Templateid{
			Templateid: v,
		})
	}

	params.Params.Hostid = hostid
	params.Params.Templates = append(params.Params.Templates, Templateid{
		Templateid: templateid,
	})

	return params, 0

}

func (c *HostUpdateTemplateCommand) HostUpdateTemplateAPI(args []string) (*resHostUpdateTemplate, int) {

	params, ret := c.makeParams(args)
	if ret != 0 {
		return &resHostUpdateTemplate{}, ret
	}

	res, err := zabbixRequest(c.Meta.URL, params)
	if err != nil {
		c.Ui.Error(fmt.Sprintf("zabbixRequest: %s", err))
		return &resHostUpdateTemplate{}, 1
	}

	var data *resHostUpdateTemplate
	perr := json.Unmarshal([]byte(res), &data)
	if perr != nil {
		c.Ui.Error(fmt.Sprintf("Error json.Unmarshal: %s", perr))
		return data, 1
	}

	return data, 0
}

func (c *HostUpdateTemplateCommand) Run(args []string) int {

	_, ret := c.HostUpdateTemplateAPI(args)
	if ret != 0 {
		return ret
	}

	return 0
}

func (c *HostUpdateTemplateCommand) Synopsis() string {
	return "add template link"
}

func (c *HostUpdateTemplateCommand) Help() string {
	helpText := `
Usage: 
   host update template [HOST_NAME] [TEMPLATE_NAME]

`
	return strings.TrimSpace(helpText)
}
