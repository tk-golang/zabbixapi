package command

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"net/url"
	"os"
	"testing"

	"github.com/mitchellh/cli"
)

var meta *Meta

type reqTEST struct {
	Auth    string `json:"auth"`
	Jsonrpc string `json:"jsonrpc"`
	Method  string `json:"method"`
	ID      int    `json:"id"`
	Params  struct {
		Output                []string `json:"output"`
		SelectInterfaces      []string `json:"selectInterfaces"`
		SelectGroups          []string `json:"selectGroups"`
		SelectParentTemplates []string `json:"selectParentTemplates"`
		Filter                struct {
			Host []string `json:"host"`
			Name []string `json:"name"`
		} `json:"filter"`
		Hostids string `json:"hostids"` // template get
	} `json:"params"`
}

func TestMain(m *testing.M) {

	// ExampleLoadConfig()
	os.Setenv("HOME", "./test")

	// Meta
	_, apikey, _ := Login()
	config, _ := LoadConfig()
	meta = &Meta{
		Ui: &cli.ColoredUi{
			InfoColor:  cli.UiColorBlue,
			ErrorColor: cli.UiColorRed,
			Ui: &cli.BasicUi{
				Writer:      os.Stdout,
				ErrorWriter: os.Stderr,
				Reader:      os.Stdin,
			},
		},
		Key: apikey,
		URL: config.URL,
	}

	// zabbixRequest の http.NewRequest を Mock する
	dummy := http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		var s *reqTEST
		body, _ := ioutil.ReadAll(r.Body)
		json.Unmarshal(body, &s)
		w.WriteHeader(http.StatusOK)
		switch s.Method {
		case "user.login":
			fmt.Fprintf(w, "0424bd59b807674191e7d77572075f33")
		case "host.get":
			fmt.Fprintf(w, `{"jsonrpc":"2.0","result":[{"hostid":"10160","parentTemplates":[{"name":"Linux","templateid":"10001"},{"name":"httpd","templateid":"10080"}]}]}`)
		case "host.delete":
			fmt.Fprintf(w, `{"jsonrpc":"2.0","result":{"hostids":["10170"]},"id":1}`)
		case "host.update":
			fmt.Fprintf(w, `{"jsonrpc":"2.0","result":{"hostids":["10180"]},"id":1}`)
		case "hostgroup.get":
			fmt.Fprintf(w, `{"jsonrpc":"2.0","result":[{"groupid":"22","name":"Linux servers"}]}`)
			/*
				if len(s.Params.Filter.Name) > 1 {
					fmt.Fprintf(w, `{"jsonrpc":"2.0","result":[{"groupid":"22","name":"Linux servers"},{"groupid":"23","name":"redis"}]}`)
				} else {
					fmt.Fprintf(w, `{"jsonrpc":"2.0","result":[{"groupid":"22","name":"Linux servers"}]}`)
				}
			*/
		case "template.get":
			fmt.Fprintf(w, `{"jsonrpc":"2.0","result":[{"templateid":"10001","name":"httpd"},{"templateid":"11000","name":"db"}]}`)
		case "graph.get":
			fmt.Fprintf(w, `{"jsonrpc":"2.0","result":[{"graphid":"612","name":"CPU jumps"},{"graphid": "613","name": "CPU load"}]}`)
		case "screen.get":
			//fmt.Fprintf(w, `{"jsonrpc":"2.0","result":[],"screenid":"26","name":"CPU Graph","hsize":"3","vsize":"2"}`)
			//fmt.Fprintf(w, `{"jsonrpc":"2.0","result":[{"screenitems":[{"screenitemid":"67","screenid":"26"}],"screenid":"26","name":"CPUGraphs","hsize":"3","vsize":"2","templateid":"0","userid":"1","private":"1"}],"id":1}`)

			if s.Params.Filter.Name[0] == "testscreen" {
				fmt.Fprintf(w, `{"jsonrpc":"2.0","result":[],"screenid":"26","name":"CPU Graph","hsize":"3","vsize":"2"}`)
			} else {
				fmt.Fprintf(w, `{"jsonrpc":"2.0","result":[{"screenitems":[{"screenitemid":"67","screenid":"26"}],"screenid":"26","name":"CPUGraphs","hsize":"3","vsize":"2","templateid":"0","userid":"1","private":"1"}],"id":1}`)
			}

		case "screen.delete":
			fmt.Fprintf(w, `{"jsonrpc":"2.0","result":{"screenids":["25",]},"id":1}`)
		case "screen.create":
			fmt.Fprintf(w, `{"jsonrpc": "2.0","result": {"screenids": ["26"]},"id": 1}`)
		default:
			fmt.Fprintf(w, "empty")
		}

	})
	ts := httptest.NewServer(dummy)

	defer ts.Close()
	defaultProxy := http.DefaultTransport.(*http.Transport).Proxy
	http.DefaultTransport.(*http.Transport).Proxy = func(req *http.Request) (*url.URL, error) {
		// テストサーバのURLをProxyに設定
		return url.Parse(ts.URL)
	}
	// テストが終わったらProxyを元に戻す
	defer func() { http.DefaultTransport.(*http.Transport).Proxy = defaultProxy }()

	exitCode := m.Run()
	os.Exit(exitCode)

}
