package command

import (
	"fmt"
)

func ExampleHostGetCommand_makeParams() {

	var c *HostGetCommand
	c = &HostGetCommand{
		Meta: *meta,
	}

	req := c.makeParams([]string{})
	fmt.Printf("%+v\n", req)

	req = c.makeParams([]string{"test"})
	fmt.Printf("%+v", req.Params.Filter.Host)

	// Output:
	// &{Auth: Jsonrpc:2.0 Method:host.get ID:1 Params:{Output:[host available] SelectInterfaces:[ip] SelectGroups:[name] SelectParentTemplates:[templateid name] Filter:{Host:[]}}}
	// [test]

}

func ExampleHostGetCommand_HostGetID() {
	c := &HostGetCommand{
		Meta: *meta,
	}
	host, _ := c.HostGetID("test")
	fmt.Println(host)

	// Output: 10160
}

func ExampleHostGetCommand_HostGetAPI() {

	var c *HostGetCommand
	c = &HostGetCommand{
		Meta: *meta,
	}

	res, _ := c.HostGetAPI([]string{"test"})
	fmt.Printf("%+v\n", res)

	// Output:
	// {Jsonrpc:2.0 Result:[{Hostid:10160 Host: Available: Groups:[] Interfaces:[] ParentTemplates:[{Name:Linux Templateid:10001} {Name:httpd Templateid:10080}]}] ID:0}

}
