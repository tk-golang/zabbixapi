package command

import "fmt"

func ExampleHostUpdateTemplateCommand_makeParams() {

	c := &HostUpdateTemplateCommand{
		Meta: *meta,
	}

	// ng test
	c.makeParams([]string{})

	// ok test
	params, _ := c.makeParams([]string{"host", "hosttemplate"})
	fmt.Printf("%+v\n", params)

	// Output:
	// host update template [HOST_NAME] [TEMPLATE_NAME]
	// &{Auth: Jsonrpc:2.0 Method:host.update ID:1 Params:{Hostid:10160 Templates:[{Templateid:10001} {Templateid:10080} {Templateid:10001}]}}

}

func ExampleHostUpdateTemplateCommand_HostUpdateTemplateAPI() {
	c := &HostUpdateTemplateCommand{
		Meta: *meta,
	}

	data, _ := c.HostUpdateTemplateAPI([]string{"www01", "Linux"})
	fmt.Printf("%+v\n", data)

	// Output:
	// &{Result:{Hostids:[10180]}}
}
