package command

import "fmt"

func ExampleTemplateGetCommand_makeParams() {

	c := &TemplateGetCommand{
		Meta: *meta,
	}

	// len(args) != 1
	//c.makeParams([]string{})

	data, _ := c.makeParams([]string{"www01"})
	fmt.Printf("%+v\n", data)

	// Output:
	// &{Auth: Jsonrpc:2.0 Method:template.get ID:1 Params:{Hostids: Filter:{Host:[www01]}}}

}

func ExampleTemplateGetCommand_GetTemplateID() {

	c := &TemplateGetCommand{
		Meta: *meta,
	}
	data, _ := c.GetTemplateID("www01")
	fmt.Printf("%+v\n", data)

	// Output:
	// 10001

}

func ExampleTemplateGetCommand_GetTemplateIDs() {

	c := &TemplateGetCommand{
		Meta: *meta,
	}
	data, _ := c.GetTemplateIDs("www01")
	fmt.Printf("%+v\n", data)

	// Output:
	// [10001 10080]

}
