package command

import (
	"encoding/json"
	"fmt"
	"strconv"
	"strings"
)

type ScreenCreateCommand struct {
	Meta
}

type reqScreenCreate struct {
	Auth    string `json:"auth"`
	Jsonrpc string `json:"jsonrpc"`
	Method  string `json:"method"`
	ID      int    `json:"id"`
	Params  struct {
		Name        string       `json:"name"`
		Hsize       int          `json:"hsize"`
		Vsize       int          `json:"vsize"`
		Screenitems []Screenitem `json:"screenitems"`
	} `json:"params"`
}

type Screenitem struct {
	Resourcetype int `json:"resourcetype"`
	Resourceid   int `json:"resourceid"`
	X            int `json:"x"`
	Y            int `json:"y"`
	Height       int `json:"height"`
	Width        int `json:"width"`
}

type resScreenCreate struct {
	Jsonrpc string `json:"jsonrpc"`
	Result  struct {
		Screenids []string `json:"screenids"`
	} `json:"result"`
	ID    int `json:"id"`
	Error struct {
		Code    int    `json:"code"`
		Message string `json:"message"`
		Data    string `json:"data"`
	}
}

func (c *ScreenCreateCommand) makeParams(args []string) (*reqScreenCreate, int) {

	if len(args) == 0 {
		fmt.Println("screen create [SCREEN_NAME] [HOST_NAME...]")
		return &reqScreenCreate{}, 1
	}
	name := args[0]

	hostgraph := make([][]string, len(args[1:]))
	for k, host := range args[1:] {
		g := &GraphGetCommand{
			Meta: c.Meta,
		}
		data, ret := g.GraphGetids(host)
		if ret != 0 {
			return &reqScreenCreate{}, ret
		}
		hostgraph[k] = data
	}

	// screen check
	s := &ScreenGetCommand{
		Meta: c.Meta,
	}
	ret := s.ScreenCheck(name)
	if ret != 0 {
		return &reqScreenCreate{}, ret
	}

	// screen create
	params := &reqScreenCreate{
		Jsonrpc: JSONRPC,
		Method:  SCREEN_CREATE_METHOD,
		ID:      ID,
		Auth:    c.Meta.Key,
	}
	params.Params.Name = name

	// 横軸X: ホスト
	// 縦軸Y: グラフ
	// Y 軸は一番多いホストに合わせる
	var xc, yc, ym int
	for x, hostkey := range hostgraph {
		for y, graphid := range hostkey {
			i, _ := strconv.Atoi(graphid)
			params.Params.Screenitems = append(params.Params.Screenitems, Screenitem{
				Resourcetype: 0,
				Resourceid:   i,
				Y:            y,
				X:            x,
				Height:       170,
				Width:        500,
			})
			yc++
		}
		xc++

		if yc >= ym {
			ym = yc
		}
		yc = 0
	}
	params.Params.Vsize = ym
	params.Params.Hsize = xc

	return params, 0
}

func (c *ScreenCreateCommand) Run(args []string) int {

	params, ret := c.makeParams(args)
	if ret != 0 {
		return ret
	}

	res, err := zabbixRequest(c.Meta.URL, params)
	if err != nil {
		c.Ui.Error(fmt.Sprintf("zabbixRequest: %s", err))
		return 1
	}

	var data resScreenCreate
	err = json.Unmarshal([]byte(res), &data)
	if err != nil {
		c.Ui.Error(fmt.Sprintf("json.Unmarshal: %s", err))
		return 1
	}
	if data.Error.Code != 0 {
		fmt.Printf("Code:%v Message:%v %v\n", data.Error.Code, data.Error.Message, data.Error.Data)
		return 1
	}
	fmt.Printf("ScreenID: %+v\n", data.Result.Screenids)

	return 0
}

func (c *ScreenCreateCommand) Synopsis() string {
	return "create screen with hosts"
}

func (c *ScreenCreateCommand) Help() string {
	helpText := `
Usage: 
   screen create [SCREEN_NAME] [HOST_NAME...]

`
	return strings.TrimSpace(helpText)
}
