package command

import (
	"encoding/json"
	"fmt"
	"strings"
)

type HostDeleteCommand struct {
	Meta
}

type reqHostDelete struct {
	Jsonrpc string   `json:"jsonrpc"`
	Method  string   `json:"method"`
	Params  []string `json:"params"`
	Auth    string   `json:"auth"`
	ID      int      `json:"id"`
}

type resHostDelete struct {
	Jsonrpc string `json:"jsonrpc"`
	Result  struct {
		Hostids []string `json:"hostids"`
	} `json:"result"`
	ID int `json:"id"`
}

func (c *HostDeleteCommand) hostMakeParams(args []string) (*reqHostDelete, int) {

	params := &reqHostDelete{
		Jsonrpc: JSONRPC,
		Method:  HOST_DELETE_METHOD,
		ID:      ID,
		Auth:    c.Meta.Key,
	}
	if len(args) != 1 {
		fmt.Println("host delete [HOST_NAME]")
		return params, 1
	}
	name := args[0]

	h := &HostGetCommand{
		Meta: c.Meta,
	}
	hostid, ret := h.HostGetID(name)
	if ret != 0 {
		return params, 1
	}

	params.Params = append(params.Params, hostid)

	return params, 0

}

func (c *HostDeleteCommand) Run(args []string) int {

	params, ret := c.hostMakeParams(args)
	if ret != 0 {
		return ret
	}

	res, err := zabbixRequest(c.URL, params)
	if err != nil {
		return 1
	}

	var res_u resHostDelete
	perr := json.Unmarshal([]byte(res), &res_u)
	if perr != nil {
		fmt.Println(perr)
		return 1
	}

	return 0
}

func (c *HostDeleteCommand) Synopsis() string {
	return "delete host"
}

func (c *HostDeleteCommand) Help() string {
	helpText := `
Usage:
   host delete [HOST_NAME]
`
	return strings.TrimSpace(helpText)
}
