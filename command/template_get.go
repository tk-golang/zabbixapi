package command

import (
	"encoding/json"
	"fmt"
	"strings"
)

type TemplateGetCommand struct {
	Meta
}

// https://xn--go-hh0g6u.com/pkg/encoding/json/
// Hostids 空の場合 skip される
type reqTemplateGet struct {
	Auth    string `json:"auth"`
	Jsonrpc string `json:"jsonrpc"`
	Method  string `json:"method"`
	ID      int    `json:"id"`
	Params  struct {
		Hostids string `json:",hostids"`
		Filter  struct {
			Host []string `json:"host"`
		} `json:"filter"`
	} `json:"params"`
}

type resTemplateGet struct {
	Result []struct {
		Name       string `json:"name"`
		Templateid string `json:"templateid"`
	} `json:"result"`
}

func (c *TemplateGetCommand) makeParams(args []string) (*reqTemplateGet, int) {

	//var s struct{}
	params := &reqTemplateGet{
		Jsonrpc: JSONRPC,
		Method:  TEMPLATE_GET_METHOD,
		ID:      ID,
		Auth:    c.Meta.Key,
	}

	if len(args) == 1 {
		name := args[0]
		params.Params.Filter.Host = []string{name}
		//append(params.Params.Filter, []string{name})
	}
	/*
		fmt.Printf("args = %+v\n", args)
		if len(args) == 1 {
			name := args[0]
			h := &HostGetCommand{
				Meta: c.Meta,
			}
			hostid, ret := h.HostGetID(name)
			if ret != 0 {
				return &reqTemplateGet{}, ret
			}
			params.Params.Hostids = hostid
		}
	*/
	//fmt.Printf("hostid = %+v\n", hostid)
	//fmt.Printf("params = %+v\n", params)

	return params, 0
}

func (c *TemplateGetCommand) GetTemplateID(name string) (string, int) {

	data, ret := c.TemplateGetAPI([]string{name})
	if ret != 0 {
		c.Ui.Error(fmt.Sprintf("TemplateGetAPI: %s", name))
		return "", 1
	}
	if len(data.Result) == 0 {
		c.Ui.Error(fmt.Sprintf("Template does not exist: %s", name))
		return "", 1
	}
	return data.Result[0].Templateid, ret
}

func (c *TemplateGetCommand) GetTemplateIDs(name string) ([]string, int) {

	h := &HostGetCommand{
		Meta: c.Meta,
	}
	data, ret := h.HostGetAPI([]string{name})
	if ret != 0 {
		c.Ui.Error(fmt.Sprintf("HostGetAPI: %s", name))
		return []string{}, 1
	}
	if len(data.Result) == 0 {
		c.Ui.Error(fmt.Sprintf("Host does not exist: %s", name))
		return []string{}, 1
	}
	//fmt.Printf("data = %+v\n", data)

	templateids := []string{}
	for _, v := range data.Result[0].ParentTemplates {
		templateids = append(templateids, v.Templateid)
	}

	return templateids, ret
}

func (c *TemplateGetCommand) TemplateGetAPI(args []string) (*resTemplateGet, int) {

	params, ret := c.makeParams(args)
	if ret != 0 {
		return &resTemplateGet{}, ret
	}

	res, err := zabbixRequest(c.Meta.URL, params)
	if err != nil {
		c.Ui.Error(fmt.Sprintf("zabbixRequest: %s", err))
		return &resTemplateGet{}, 1
	}

	var data *resTemplateGet
	err = json.Unmarshal([]byte(res), &data)
	if err != nil {
		c.Ui.Error(fmt.Sprintf("Error json.Unmarshal: %s", err))
		return &resTemplateGet{}, 1
	}

	return data, 0
}

func (c *TemplateGetCommand) Run(args []string) int {

	data, ret := c.TemplateGetAPI(args)
	if ret != 0 {
		return ret
	}

	fmt.Printf("%-5s %-15s\n", "ID", "Template")
	for _, v := range data.Result {
		fmt.Printf("%-5s %-15s\n", v.Templateid, v.Name)
	}

	return 0
}

func (c *TemplateGetCommand) Synopsis() string {
	return "template get [HOST_NAME]"
}

func (c *TemplateGetCommand) Help() string {
	helpText := `
Usage:

   * template get
     template list

   * template get [HOST_NAME]
     host's templates list

     
`
	return strings.TrimSpace(helpText)
}
