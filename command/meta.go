package command

import "github.com/mitchellh/cli"

const (
	ExitCodeOK                int = 0
	ExitCodeError             int = 1 + iota
	USER_LOGIN_METHOD             = "user.login"
	HOST_GET_METHOD               = "host.get"
	HOST_DELETE_METHOD            = "host.delete"
	HOST_UPDATE_METHOD            = "host.update"
	TEMPLATE_GET_METHOD           = "template.get"
	GRAPH_GET_METHOD              = "graph.get"
	SCREEN_CREATE_METHOD          = "screen.create"
	SCREEN_GET_METHOD             = "screen.get"
	SCREEN_DELETE_METHOD          = "screen.delete"
	SCREEN_ITEM_CREATE_METHOD     = "screenitem.create"
	GROUP_GET_METHOD              = "hostgroup.get"
	JSONRPC                       = "2.0"
	ID                            = 1
)

// Meta contain the meta-option that nearly all subcommand inherited.
type Meta struct {
	Ui cli.Ui
	//Ui  cli.BasicUi
	Key string
	URL string
}
