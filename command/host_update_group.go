package command

import (
	"encoding/json"
	"fmt"
	"strings"
)

type HostUpdateGroupCommand struct {
	Meta
}

type reqHostUpdateGroup struct {
	Auth    string `json:"auth"`
	Jsonrpc string `json:"jsonrpc"`
	Method  string `json:"method"`
	ID      int    `json:"id"`
	Params  struct {
		Hostid string    `json:"hostid"`
		Groups []Groupid `json:"groups"`
	} `json:"params"`
}

type Groupid struct {
	Groupid string `json:"groupid"`
}

type resHostUpdateGroup struct {
	Result struct {
		Hostids []string `json:"hostids"`
	} `json:"result"`
}

func (c *HostUpdateGroupCommand) hostMakeParams(args []string) (*reqHostUpdateGroup, int) {

	params := &reqHostUpdateGroup{
		Jsonrpc: JSONRPC,
		Method:  HOST_UPDATE_METHOD,
		ID:      ID,
		Auth:    c.Meta.Key,
	}
	if len(args) != 2 {
		fmt.Println("host update group [HOST_NAME] [GROUP_NAME]")
		return params, 1
	}

	// get host id
	name := args[0]
	h := &HostGetCommand{
		Meta: c.Meta,
	}
	hostid, ret := h.HostGetID(name)
	if ret != 0 {
		return params, ret
	}

	// get group id
	group_name := args[1]
	g := &GroupGetCommand{
		Meta: c.Meta,
	}
	groupid, ret := g.GetGroupID(group_name)
	if ret != 0 {
		return params, ret
	}

	params.Params.Hostid = hostid
	params.Params.Groups = append(params.Params.Groups, Groupid{
		Groupid: groupid,
	})

	return params, 0
}

func (c *HostUpdateGroupCommand) HostUpdateGroupAPI(args []string) int {

	var data resHostUpdateGroup
	params, ret := c.hostMakeParams(args)
	if ret != 0 {
		return ret
	}

	res, err := zabbixRequest(c.Meta.URL, params)
	if err != nil {
		c.Ui.Error(fmt.Sprintf("zabbixRequest: %s", err))
		return 1
	}

	perr := json.Unmarshal([]byte(res), &data)
	if perr != nil {
		c.Ui.Error(fmt.Sprintf("Error json.Unmarshal: %s", perr))
		return 1
	}

	return 0
}

func (c *HostUpdateGroupCommand) Run(args []string) int {
	return (c.HostUpdateGroupAPI(args))
}

func (c *HostUpdateGroupCommand) Synopsis() string {
	return "add group link"
}

func (c *HostUpdateGroupCommand) Help() string {
	helpText := `
Usage: 
   host update group [HOST_NAME] [GROUP_NAME]

`
	return strings.TrimSpace(helpText)
}
