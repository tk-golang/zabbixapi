package command

import "fmt"

func ExampleHostDeleteCommand_hostMakeParams() {

	var c *HostDeleteCommand
	c = &HostDeleteCommand{
		Meta: *meta,
	}

	// ng test
	c.hostMakeParams([]string{})

	// ok test
	req, _ := c.hostMakeParams([]string{"test"})
	fmt.Printf("%+v\n", req)

	// Output:
	// host delete [HOST_NAME]
	// &{Jsonrpc:2.0 Method:host.delete Params:[10160] Auth: ID:1}

}
