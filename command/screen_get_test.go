package command

import "fmt"

func ExampleScreenGetCommand_ScreenCheck() {

	c := &ScreenGetCommand{
		Meta: *meta,
	}
	data, _ := c.makeParams([]string{})
	fmt.Printf("%+v\n", data)

	// Output:
	// &{Auth: Jsonrpc:2.0 Method:screen.get ID:1 Params:{Output:extend Filter:{Screenid:[] Name:[]}}}

}

func ExampleScreenGetCommand_ScreenGetID() {

	c := &ScreenGetCommand{
		Meta: *meta,
	}
	data, _ := c.ScreenGetID("")
	fmt.Printf("%+v\n", data)

	// Output:
	// 26

}

func ExampleScreenGetCommand_ScreenGetAPI() {
	c := &ScreenGetCommand{
		Meta: *meta,
	}

	data, _ := c.ScreenGetAPI([]string{"test"})
	fmt.Printf("%+v\n", *data)

	// Output:
	// {Jsonrpc:2.0 ID:1 Result:[{Screenid:26 Name:CPUGraphs}]}
}
