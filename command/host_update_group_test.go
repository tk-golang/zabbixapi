package command

import "fmt"

func ExampleHostUpdateGroupCommand_hostMakeParams() {

	var c *HostUpdateGroupCommand
	c = &HostUpdateGroupCommand{
		Meta: *meta,
	}

	// ng test
	c.hostMakeParams([]string{})

	// ok test
	params, _ := c.hostMakeParams([]string{"test", "testgroup"})
	fmt.Printf("%+v\n", params)

	// Output:
	// host update group [HOST_NAME] [GROUP_NAME]
	// &{Auth: Jsonrpc:2.0 Method:host.update ID:1 Params:{Hostid:10160 Groups:[{Groupid:22}]}}

}
