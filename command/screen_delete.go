package command

import (
	"encoding/json"
	"fmt"
	"strings"
)

type ScreenDeleteCommand struct {
	Meta
}

type reqScreenDelete struct {
	Jsonrpc string   `json:"jsonrpc"`
	Method  string   `json:"method"`
	Params  []string `json:"params"`
	Auth    string   `json:"auth"`
	ID      int      `json:"id"`
}

type resScreenDelete struct {
	Jsonrpc string `json:"jsonrpc"`
	Result  struct {
		Screenids []string `json:"screenids"`
	} `json:"result"`
	ID int `json:"id"`
}

func (c *ScreenDeleteCommand) Run(args []string) int {

	if len(args) == 0 {
		fmt.Println("screen create NAME")
		return 10
	}
	name := args[0]
	s := &ScreenGetCommand{
		Meta: c.Meta,
	}
	screen_id, ret := s.ScreenGetID(name)
	if ret != 0 {
		return ret
	}

	params := &reqScreenDelete{
		Jsonrpc: JSONRPC,
		Method:  SCREEN_DELETE_METHOD,
		ID:      ID,
		Auth:    c.Meta.Key,
	}

	params.Params = append(params.Params, screen_id)

	res, err := zabbixRequest(c.Meta.URL, params)
	if err != nil {
		c.Ui.Error(fmt.Sprintf("zabbixRequest: %s", err))
		return 1
	}

	var res_u resScreenDelete
	perr := json.Unmarshal([]byte(res), &res_u)
	if perr != nil {
		c.Ui.Error(fmt.Sprintf("json unmarshal: %s", err))
		return 1
	}

	return 0
}

func (c *ScreenDeleteCommand) Synopsis() string {
	return "delete screen"
}

func (c *ScreenDeleteCommand) Help() string {
	helpText := `
Usage: 
   screen delete [SCREEN_NAME]

`
	return strings.TrimSpace(helpText)
}
