package command

import "fmt"

func ExampleGraphGetCommand_GraphGetids() {

	c := &GraphGetCommand{
		Meta: *meta,
	}
	data, _ := c.GraphGetids("host")
	fmt.Printf("%+v\n", data)

	// Output:
	// [612 613]

}

func ExampleGraphGetCommand_makeParams() {
	c := &GraphGetCommand{
		Meta: *meta,
	}

	// ng test
	c.makeParams([]string{})

	// ok test
	data, _ := c.makeParams([]string{"host"})
	fmt.Printf("%+v\n", data)

	// Output:
	// graph get [HOST_NAME]
	// &{Auth: Jsonrpc:2.0 Method:graph.get ID:1 Params:{Hostids:10160}}

}

func ExampleGraphGetCommand_GraphGetAPI() {

	var c *GraphGetCommand
	c = &GraphGetCommand{
		Meta: *meta,
	}
	data, _ := c.GraphGetAPI([]string{"test"})
	fmt.Printf("%+v\n", data)

	// Output:
	// &{Result:[{Graphid:612 Name:CPU jumps} {Graphid:613 Name:CPU load}]}

}
