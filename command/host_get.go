package command

import (
	"encoding/json"
	"fmt"
	"strings"
)

type HostGetCommand struct {
	Meta
}

type reqHostGet struct {
	Auth    string `json:"auth"`
	Jsonrpc string `json:"jsonrpc"`
	Method  string `json:"method"`
	ID      int    `json:"id"`
	Params  struct {
		Output                []string `json:"output"`
		SelectInterfaces      []string `json:"selectInterfaces"`
		SelectGroups          []string `json:"selectGroups"`
		SelectParentTemplates []string `json:"selectParentTemplates"`
		Filter                struct {
			//Available int      `json:"available"`
			Host []string `json:"host"`
		} `json:"filter"`
	} `json:"params"`
}

type resHostGet struct {
	Jsonrpc string `json:"jsonrpc"`
	Result  []struct {
		Hostid    string `json:"hostid"`
		Host      string `json:"host"`
		Available string `json:"available"`
		Groups    []struct {
			Groupid string `json:"groupid"`
			Name    string `json:"name"`
		} `json:"groups"`
		Interfaces []struct {
			IP string `json:"ip"`
		} `json:"interfaces"`
		ParentTemplates []struct {
			Name       string `json:"name"`
			Templateid string `json:"templateid"`
		} `json:"parentTemplates"`
	} `json:"result"`
	ID int `json:"id"`
}

func (c *HostGetCommand) HostGetID(name string) (string, int) {

	data, ret := c.HostGetAPI([]string{name})
	if ret != 0 {
		c.Ui.Error(fmt.Sprintf("HostGetAPI: %s", name))
		return "", 1
	}

	if len(data.Result) == 0 {
		c.Ui.Error(fmt.Sprintf("Host does not exist: %s", name))
		return "", 1
	}
	return data.Result[0].Hostid, ret
}

func (c *HostGetCommand) makeParams(args []string) *reqHostGet {

	var params *reqHostGet
	params = &reqHostGet{
		Jsonrpc: JSONRPC,
		Method:  HOST_GET_METHOD,
		ID:      ID,
		Auth:    c.Meta.Key,
	}
	params.Params.Output = append(params.Params.Output, "host")
	params.Params.Output = append(params.Params.Output, "available")
	params.Params.SelectInterfaces = append(params.Params.SelectInterfaces, "ip")
	params.Params.SelectGroups = append(params.Params.SelectGroups, "name")
	params.Params.SelectParentTemplates = append(params.Params.SelectParentTemplates, "templateid")
	params.Params.SelectParentTemplates = append(params.Params.SelectParentTemplates, "name")

	if len(args) == 1 {
		host := args[0]
		params.Params.Filter.Host = append(params.Params.Filter.Host, host)
	}

	return params

}

func (c *HostGetCommand) HostGetAPI(args []string) (resHostGet, int) {

	params := c.makeParams(args)

	var data resHostGet
	res, err := zabbixRequest(c.Meta.URL, params)
	if err != nil {
		c.Ui.Error(fmt.Sprintf("zabbixRequest: %s", err))
		return data, 1
	}

	perr := json.Unmarshal([]byte(res), &data)
	if perr != nil {
		c.Ui.Error(fmt.Sprintf("Error json.Unmarshal: %s", err))
		return data, 1
	}

	return data, 0
}

func (c *HostGetCommand) Run(args []string) int {

	data, ret := c.HostGetAPI(args)
	if ret != 0 {
		return ret
	}

	if len(data.Result) == 0 {
		return 0
	}

	fmt.Printf("%-5s %-20s %-15s %-5s %-20s %-5s\n", "ID", "Host", "IP", "GID", "Group", "Available")
	for _, v := range data.Result {
		fmt.Printf("%-5s %-20s %-15s %-5s %-20s %-5s\n",
			v.Hostid, v.Host, v.Interfaces[0].IP, v.Groups[0].Groupid, v.Groups[0].Name, v.Available)
	}

	return 0
}

func (c *HostGetCommand) Synopsis() string {
	return "get host info"
}

func (c *HostGetCommand) Help() string {
	helpText := `
Usage: 
   * single host 
     host get [HOSTNAME]

   * all host
     host get

`
	return strings.TrimSpace(helpText)
}
