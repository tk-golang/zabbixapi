package command

import (
	"encoding/json"
	"errors"
	"fmt"
	"gopkg.in/yaml.v2"
	"io/ioutil"
	"os"
	"path/filepath"
)

//// Zabbix API Request/Response JSON 構造体 ////
/*
  user.login Request, Response
  https://www.zabbix.com/documentation/4.2/manual/api/reference/user/login
*/

type reqLogin struct {
	Jsonrpc string `json:"jsonrpc"`
	Method  string `json:"method"`
	ID      int    `json:"id"`
	Params  struct {
		User     string `json:"user"`
		Password string `json:"password"`
	} `json:"params"`
}

// user.login のレスポンス受信構造体
type resLogin struct {
	Jsonrpc string `json:"jsonrpc"`
	Result  string `json:"result"`
	ID      int    `json:"id"`
}

// AuthConfig  ログイン情報
type AuthConfig struct {
	URL      string `yaml:"url"`
	User     string `yaml:"user"`
	Password string `yaml:"password"`
}

/*
Login ログインして apikey 取得
   値の定義と関数呼び出しのみなのでテスト不要
   LoadConfig()
   zabbixRequest()
   loginUnmarshal()
   を個別でテストする
*/
func Login() (AuthConfig, string, error) {

	// ~/.config/zabbix.yml
	config, ret := LoadConfig()
	if ret != 0 {
		//c.Ui.Error(fmt.Sprintf("Login: %s", "config"))
		return config, "", errors.New("")
	}

	// API KEY 取得
	/*
			  Params の定義を外に出しておけば
			  Params: &Params {}
		      のようにデータを入れられる
	*/
	params := &reqLogin{
		Jsonrpc: JSONRPC,
		Method:  USER_LOGIN_METHOD,
		ID:      ID,
	}
	params.Params.User = config.User
	params.Params.Password = config.Password
	res, err := zabbixRequest(config.URL, params)
	if err != nil {
		fmt.Errorf("%s\n", err)
		return config, "", err
	}

	// レスポンス json 解析
	apikey, err := loginUnmarshal(res)
	if err != nil {
		fmt.Errorf("%s\n", err)
		return config, "", err
	}

	// apikey
	return config, apikey, nil
}

// login.auth レスとポンス JSON 解析
func loginUnmarshal(res string) (string, error) {
	var resLogin resLogin
	err := json.Unmarshal([]byte(res), &resLogin)
	if err != nil {
		fmt.Errorf("%s\n", err)
		return "", err
	}

	if resLogin.Result == "" {
		fmt.Errorf("%s\n", err)
		return "", nil
	}
	return resLogin.Result, nil
}

// loadConfig  ~/.config/zabbixapi.yml から認証情報読み取り
func LoadConfig() (AuthConfig, int) {
	var config AuthConfig
	home := os.Getenv("HOME")

	configFile := filepath.Join(home, ".config", "zabbixapi.yml")

	buf, err := ioutil.ReadFile(configFile)

	if err != nil {
		//fmt.Errorf("Config.ReadYaml> fail to read: %v", err)
		fmt.Printf("Config.ReadYaml> fail to read: %v", err)
		return config, 1
	}

	err = yaml.Unmarshal([]byte(buf), &config)
	if err != nil {
		//fmt.Errorf("yaml.Unmarshal> fail to read: %v", err)
		fmt.Printf("yaml.Unmarshal> fail to read: %v", err)
		return config, 1
	}
	return config, 0
}
