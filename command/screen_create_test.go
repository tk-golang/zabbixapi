package command

func ExampleScreenCreateCommand_makeParams() {

	c := &ScreenCreateCommand{
		Meta: *meta,
	}
	c.makeParams([]string{})

	// Output:
	// screen create [SCREEN_NAME] [HOST_NAME...]
}

func ExampleScreenCreateCommand_Run() {
	c := &ScreenCreateCommand{
		Meta: *meta,
	}

	c.Run([]string{})

	c.Run([]string{"testscreen", "www01", "www02"})

	// Output:
	// screen create [SCREEN_NAME] [HOST_NAME...]
	// ScreenID: [26]
}
